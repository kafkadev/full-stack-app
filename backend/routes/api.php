<?php
use Illuminate\Http\Request;
$app->get('/', 'StartController@index');


$app->post('/postapilogin2', function (){
	$currentForm = app('request')->all();

	return response()->json($currentForm, 200);

});

$app->post('/demo-return', function (){
	$currentForm = app('request')->all();
	$file_name = "default";
	if ($currentForm['imaj']) {
		$file_name = bannerUpload($currentForm['imaj']);
	}

	//bannerUpload($currentForm['files']);
	//$currentForm['imaj']->getClientOriginalName()
	return response()->json($file_name, 200);
});

$app->get('/insert', function () use ($app) {
	$arr = array();
	for ($i=1;$i<=101;$i++) {
		$arr[] = $i;
	}
	shuffle($arr);
	foreach ($arr as $key) {
		$insert_array = [];
		$insert_array["title"] = "bir indirim - " . $key;
		$insert_array["user_id"] = 1;
		$insert_array = app('db')->table('discounts')->insertGetId($insert_array);
	}

	return response()->json($insert_array, 200);
});

$app->get('/list/discounts', function (){
	$list_array = app('db')->table('discounts')->paginate(10);
	return response()->json($list_array, 200);
});

$app->get('/report-click/{ad_id}', function ($ad_id){
	app('db')->table('report')->where('ad_id', $ad_id)->increment('click');
});

//$app->post('postlogin', 'AuthenticateController@postLogin');
$app->post('/postapilogin', 'UserController@login');

$app->group(['middleware' => 'auth:api' ], function($app)  {
	$app->get('panel', function () use ($app) {
		return "giris aktif";
	});
});
