<?php
require_once __DIR__.'/../vendor/autoload.php';
try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}
$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();
$app->withEloquent();

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);
$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->middleware([
App\Http\Middleware\CorsMiddleware::class
]);

/*
remove comment for filesystem service *Storage, *config/filesystem
$app->singleton('filesystem', function ($app) {
    return $app->loadComponent('filesystems', Illuminate\Filesystem\FilesystemServiceProvider::class, 'filesystem');
});
$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);
//remove comment for redis service
$app->register(Illuminate\Redis\RedisServiceProvider::class);
*/

$app->routeMiddleware([
     'auth' => App\Http\Middleware\Authenticate::class,
     //'can' => App\Http\Middleware\AuthToken::class,
]);

$app->register(App\Providers\AuthServiceProvider::class);

//$app->configure('database');
//$app->configure('cache');
//$app->configure('filesystems');
$app->group(['prefix' => 'api','namespace' => 'App\Http\Controllers\Api'], function ($app) {
    require __DIR__.'/../routes/api.php';
});
$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../routes/web.php';
});

return $app;


/*

*/
