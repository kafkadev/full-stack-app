/* @flow */
 
import React from "react";

import { Platform } from "react-native";
import { Root } from "native-base";
import { StackNavigator } from "react-navigation";

import Drawer from "./Drawer";
import BasicCard from "./components/card/basic";
import NHCardImage from "./components/card/card-image";
import NHCardShowcase from "./components/card/card-showcase";
import NHCardList from "./components/card/card-list";
import NHCardHeaderAndFooter from "./components/card/card-header-and-footer";
const AppNavigator = StackNavigator(
	{
		Drawer: { screen: Drawer },
		BasicCard: { screen: BasicCard },
		NHCardImage: { screen: NHCardImage },
	},
	{
		initialRouteName: "Drawer",
		headerMode: "none",
	}
);

export default () =>
	<Root>
		<AppNavigator />
	</Root>;
