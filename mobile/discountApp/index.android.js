/*
import React, { Component } from 'react';
import { Root } from "native-base";
import {
  StyleSheet,
  View,
  ActivityIndicator, ListView, Image
} from 'react-native';
import { AppRegistry } from 'react-native';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Text,
  Thumbnail,
  Left,
  Body,
  Right,
  IconNB
} from "native-base";

import styles from "./card/styles";
import NHCardImage from "./card/card-image";

const logo = require("./img/logo.png");
//const cardImage = require("http://loremflickr.com/320/240/brazil,rio");
const cardImage = require("./img/drawer-cover.png");

export default class discountApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }
  componentDidMount() {
    return fetch('http://hoppi.liberyen.com/api/list/discounts')
      .then((response) => response.json())
      .then((responseJson) => {
        let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.setState({
          isLoading: false,
          dataSource: ds.cloneWithRows(responseJson.data),
        }, function () {
          // do something with new state
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 50 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <Root>
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button> 
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Card Image</Title>
          </Body>
          <Right />
        </Header>
<NHCardImage datalar={this.state.dataSource} />

      </Container>
      </Root>
    );
  }
}
AppRegistry.registerComponent('discountApp', () => discountApp);
*/
import { AppRegistry } from 'react-native';
import App from './App';
AppRegistry.registerComponent('discountApp', () => App);